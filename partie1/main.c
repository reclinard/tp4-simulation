#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "mt.h"

/*
* lonardo()
* Simulation, sur *nb_months* mois
* Entrée : int nb_months
* Sortie : void
*/
void lonardo(int nb_months)
{
	int nb_couple_lapin = 1;
	int nb_couple_lapin_jeune = 0;
	int temp = 0;
	printf("%d %d\n", nb_couple_lapin, nb_couple_lapin_jeune);
	for (int i = 0; i < nb_months; i++)
	{
		temp = nb_couple_lapin;
		nb_couple_lapin += nb_couple_lapin_jeune;
		nb_couple_lapin_jeune = temp;

		printf("%d %d\n", nb_couple_lapin, nb_couple_lapin_jeune);
	}
}

/*
* main()
* Gère l'initialisation du générateur de Mersenne, et lance la simulation
* Entrée : void
* Sortie : int
*/
int main(void)
{
    // Initialisation du Générateur de Mersenne
    int i;
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    lonardo(20);

    return 0;
}
