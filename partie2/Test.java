package tp4.partie2;

import java.util.Random;

/*
 * Classe Test()
 * Classe qui m'a permis de vérifier expérimentalement le bon fonctionnement 
 * du taux de mortalité que je vais appliquer aux lapins
 */
class Test {
    private Random randomGenerator;
    
    /*
     * Constructeur de la classe Test
     */
    Test()
    {
        int[] seedArray = new int[]{0x123, 0x234, 0x345, 0x456};
        randomGenerator = (Random) new MTRandom(seedArray); // Mersenne Twister
    }

    /*
     * estMort()
     * Entrée : void
     * Sortie : bool
     * Simule le taux de mortalité d'un lapin sur une année, 
     * et renvoie true si le lapin est mort au cours de cette année, false sinon
     */
    private boolean estMort()
    {
        double proba_mort = 0.40;
        double p = Math.pow(1 - proba_mort, 1.0 / 12.0);
        for (int i = 0; i < 12; i++)
        {
            if (randomGenerator.nextDouble() > p)
                return true;
        }
        return false;
    }

    /*
     * testMorts()
     * Entree : void
     * Sortie : void
     * Lance la fonction estMort() un grand nombre de fois, 
     *  et affiche le taux de mortalité
     */
    private void testMorts()
    {
        int nb_morts = 0;
        int nb = 100000;
        for (int i = 0; i < nb; i++)
        {
            if (estMort())
            {
                nb_morts++;
            }
        }
        System.out.println((float)nb_morts / nb);
    }

    public static void main(String[] arg)
    {
        Test test = new Test();
        test.testMorts();
    }
}
