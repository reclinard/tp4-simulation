package tp4.partie2;


import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;

/*
 * Classe Simu()
 * Gère toute la simulation
 */
public class Simu {
    private Random randomGenerator;
    private ArrayList<Lapin> listeLapins;
    public int nbMortsJeune;
    public int nbAtteintAgeAdulte;

    /*
     * Constructeur de la classe Simu()
     */
    public Simu()
    {
        // Initialisation du générateur de Mersenne
        int[] seedArray = new int[]{0x123, 0x234, 0x345, 0x456};
        randomGenerator = (Random) new MTRandom(seedArray); // Mersenne Twister
    }

    /*
     * Réinitialise la simulation, sans réinitialiser le générateur 
     *  de nombres pseudo-aléatoires
     */
    public void initialiserSimu()
    {
        listeLapins = new ArrayList<Lapin>();

        nbMortsJeune = 0;
        nbAtteintAgeAdulte = 0;
    }

    /*
     * loiUniforme()
     * Entrée : int a, int b
     * Sortie : int
     * Retourne un nombre généré aléatoirement entre a et b, 
     *  selon la loi uniforme
     */
    public int loiUniforme(int a, int b)
    {
        return (int) (a + randomGenerator.nextDouble()*(b-a));
    }

    /*
     * loiNombrePorteesParAn()
     * Entrée : void
     * Sortie : int
     * Génère aléatoirement le nombre de portées que va avoir une lapine 
     *  au cours d'un an, selon l'histogramme défini (arbitraire)
     */
    public int loiNombrePorteesParAn()
    {
        int nbPortee = 0;

        double[] histogramme = {0.1, 0.3, 0.7, 0.9, 1};
        double nbAlea = randomGenerator.nextDouble();

        int i = 0;
        while (i < 5 && nbPortee == 0)
        {
            if (nbAlea < histogramme[i])
                nbPortee = i + 4;
            i++;
        }
        return nbPortee;
    }

    /*
     * tableauNombrePortees()
     * Entrée : int nbPortees
     * Sortie : boolean[12]
     * Génère aléatoirement le tableau des portées sur 12 mois, 
     *  à partir d'un nombre de portée
     * Dans le tableau : 
     * Si false : pas de portée ce mois
     * Si true : il y a une portée ce mois
     * Avec maximum une portée par mois
     */
    public boolean[] tableauNombrePortees(int nbPortees)
    {
        boolean[] tableau = new boolean[12]; // Rempli de false par défaut
        int indice;

        ArrayList<Integer> pasRemplis = new ArrayList<Integer>();
        for (int i = 0; i < 12; i++)
            pasRemplis.add(i);
        
        for (int i = 0; i < nbPortees; i++)
        {
            indice = loiUniforme(0, 12-i);
            tableau[pasRemplis.get(indice)] = true;
            pasRemplis.remove(indice);
        }
        return tableau;
    }

    /*
     * naissanceLapin()
     * Entrée : void
     * Sortie : Lapin
     * Retourne un nouveau Lapin, avec Sexe aléatoire
     */
    public Lapin naissanceLapin()
    {   
        Sexe sexe = Sexe.M;
        if (randomGenerator.nextDouble() > 0.5)
            sexe = Sexe.F;
        
        int ageMaturiteSexuelle = loiUniforme(5,8);

        Lapin lapin = new Lapin(sexe, ageMaturiteSexuelle);
        if (sexe == Sexe.F)
        {
            int nombrePortee = loiNombrePorteesParAn();
            lapin.setMoisDonnerNaissanceTab(tableauNombrePortees(nombrePortee));
        }

        return lapin;
    }

    /*
     * naissanceLapinChoixSexe()
     * Entrée : Sexe sexe
     * Sortie : Lapin
     * Retourne un nouveau Lapin, avec Sexe déterminé
     */
    public Lapin naissanceLapinChoixSexe(Sexe sexe)
    {
        int ageMaturiteSexuelle = loiUniforme(5,8);

        Lapin lapin = new Lapin(sexe, ageMaturiteSexuelle);
        if (sexe == Sexe.F)
        {
            int nombrePortee = loiNombrePorteesParAn();
            lapin.setMoisDonnerNaissanceTab(tableauNombrePortees(nombrePortee));
        }

        return lapin;
    }

    /*
     * iteration()
     * Entrée : void
     * Sortie : void
     * Permet de faire une itération de la simulation, 
     *  avec un pas de temps d'un mois
     */
    public void iteration()
    {
        ArrayList<Integer> indiceLapinsMorts = new ArrayList<Integer>();

        int length = this.listeLapins.size();
        for (int i = 0; i < length; i++)
        {
            Lapin lapin = this.listeLapins.get(i);

            // Naissance d'une portée
            if (lapin.getSexe() == Sexe.F && lapin.getMaturiteSexuelle())
            {
                if (lapin.naissanceCeMois())
                {
                    int nbLapinDansPortee = loiUniforme(3, 6);
                    for (int j = 0; j < nbLapinDansPortee; j++)
                    {
                        this.listeLapins.add(naissanceLapin());
                    }
                }
            }

            // Calcul de la mort ou non du Lapin
            // 1 - p = Probabilité par mois pour avoir *proba_mort%* des lapins morts 
            //  à la fin de l'année
            double p = 0;
            double proba_mort;
            if (lapin.getMaturiteSexuelle() == false)
            {
                proba_mort = 0.65;
                p = Math.pow(1 - proba_mort, 1.0 / 12);
            }
            else 
            {
                int age = lapin.getAge();
                if (age <= 24)
                {
                    proba_mort = 0.4;
                }
                else 
                {
                    proba_mort = 0.4 + ((age - 24) * (0.1 / 12.0)); 
                    // Le lapin peut donc vivre jusqu'à 8 ans max
                }
                p = Math.pow(1 - proba_mort, 1.0 / 12.0);
            }

            if (randomGenerator.nextDouble() > p)
            {
                // Le lapin est mort
                indiceLapinsMorts.add(i);
                if (!lapin.getMaturiteSexuelle())
                {
                    this.nbMortsJeune++;
                }
            }
            

            // Incrémente les attibuts de la lapine, pour gérer les naissances
            if (lapin.getSexe() == Sexe.F && lapin.getMaturiteSexuelle())
            {
                lapin.incrMoisDonnerNaissance();
                if (lapin.getMoisDonnerNaissance() == 12)
                {
                    lapin.resetMoisDonnerNaissance();
                    int nombrePortee = loiNombrePorteesParAn();
                    lapin.setMoisDonnerNaissanceTab(tableauNombrePortees(nombrePortee));
                }
            }

            // Maturité sexuelle atteinte ou non
            if (!lapin.getMaturiteSexuelle() && lapin.getAge() == lapin.getAgeMaturiteSexuelle())
            {
                this.nbAtteintAgeAdulte++;
                lapin.setMaturiteSexuelle(true);
            }

            // Incrémentation de l'âge du lapin
            lapin.incrAge();
        }

        // On enlève les lapins morts de la liste
        for (int i = indiceLapinsMorts.size() - 1; i >= 0; i--)
        {
            int indice = indiceLapinsMorts.get(i);
            this.listeLapins.remove(indice);
        }
    }

    /*
     * traiteListeLapins()
     * Compte le nombre de lapins de chaque catéorie de la liste this.listeLapins
     * Entrée : void
     * Sortie : int[] qui contient le nombre de : 
     *     jeunes_males, jeunes_femelles, adultes_males, adultes_femelles, total
     */
    private int[] traiteListeLapins()
    {
        int jeunes_males = 0;
        int jeunes_femelles = 0;
        int adultes_males = 0;
        int adultes_femelles = 0;
        int total = this.listeLapins.size();
        for (Lapin lapin : this.listeLapins)
        {
            if (lapin.getMaturiteSexuelle() == false)
            {
                if (lapin.getSexe() == Sexe.M)
                    jeunes_males++;
                else
                    jeunes_femelles++;
            }
            else
            {
                if (lapin.getSexe() == Sexe.M)
                    adultes_males++;
                else
                    adultes_femelles++;
            }
        }
        return new int[]{jeunes_males, jeunes_femelles, adultes_males, adultes_femelles, total};
    }

    /*
     * enregistreListeLapins()
     * Enregistre les données de traiteListeLapins() dans le fichier
     * Entrée : File fic
     * Sortie : void
     */
    public void enregistreListeLapins(File fic)
    {
        int[] array = traiteListeLapins();

        try (
            FileOutputStream fos = new FileOutputStream(fic, true);
            PrintStream ps = new PrintStream(fos);
        )
        {
            for (int i = 0; i < 5; i++)
                ps.print(array[i] + " ");
            ps.print("\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * enregistreListeLapins()
     * Affiche les données de traiteListeLapins()
     * Entrée : File fic
     * Sortie : void
     */
    public void afficheListeLapins()
    {
        int[] array = traiteListeLapins();
        for (int i = 0; i < 4; i++)
            System.out.print(array[i] + " ");
        System.out.println("-- " + array[4]);
    }

    /*
     * main()
     * Lance la simulation
     * Entrée : String[] arg
     * Sortie : void
     */
    public static void main(String[] arg)
    {
        Simu simu = new Simu();

        for (int k = 0; k < 30; k++)
        {
            System.out.println("----------------");
            simu.initialiserSimu();

            File fic = new File("tp4/resultats/" + k + ".txt");

            // Au début de la simulation : 20 jeunes lapins, sexe aléatoire
            for (int i = 0; i < 20; i++)
            {
                Lapin lapin = simu.naissanceLapin();
                simu.listeLapins.add(lapin);
            }

            // Au début de la simulation : un male et une femelle
            // simu.listeLapins.add(simu.naissanceLapinChoixSexe(Sexe.M));
            // simu.listeLapins.add(simu.naissanceLapinChoixSexe(Sexe.F));
            // simu.afficheListeLapins();

            simu.enregistreListeLapins(fic);

            for (int i = 0; i < 60; i++)
            {
                // System.out.print(i + " - ");
                simu.iteration();
                simu.afficheListeLapins();
                simu.enregistreListeLapins(fic);
            }
            simu.afficheListeLapins();
            
            // Affiche le taux de lapins qui n'atteingnent pas l'age adulte
            System.out.println(simu.nbMortsJeune + " " + simu.nbAtteintAgeAdulte + " " +
                simu.nbMortsJeune / (float)(simu.nbMortsJeune + simu.nbAtteintAgeAdulte));
        }
    }
}
