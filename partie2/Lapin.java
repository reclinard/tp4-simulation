package tp4.partie2;


enum Sexe {M, F};

/*
 * Classe Lapin()
 * Individu Lapin
 */
public class Lapin {
    private Sexe sexe;
    private int age = 1; // Age in months

    private boolean maturiteSexuelle = false;
    private int ageMaturiteSexuelle;

    private boolean[] moisDonnerNaissanceTab; // contient 12 éléments, pour tous les mois de l'année
    private int moisDonnerNaissance = 0;

    /*
     * Getter et/ou Setter pour les attributs age, sexe, maturiteSexuelle et ageMaturiteSexuelle
     */
    public int getAge()
    {
        return this.age;
    }

    public void incrAge()
    {
        this.age++;
    }

    public Sexe getSexe()
    {
        return this.sexe;
    }

    public boolean getMaturiteSexuelle()
    {
        return this.maturiteSexuelle;
    }

    public void setMaturiteSexuelle(boolean bool)
    {
        this.maturiteSexuelle = bool;
    }

    public int getAgeMaturiteSexuelle()
    {
        return this.ageMaturiteSexuelle;
    }

    /*
     * naissanceCeMois()
     * Entrée : void
     * Sortie : boolean - Renvoie true si la lapine donne naissance ce mois
     */
    public boolean naissanceCeMois()
    {
        return this.moisDonnerNaissanceTab[this.moisDonnerNaissance];
    }

    /*
     * getMoisDonnerNaissance()
     * Entrée : void
     * Sortie : int - Retourne l'indice du mois en cours,
     *  pour le tableau des naissances "moisDonnerNaissanceTab"
     */
    public int getMoisDonnerNaissance()
    {
        return this.moisDonnerNaissance;
    }

    /*
     * getMoisDonnerNaissanceTab()
     * Entrée : void
     * Sortie : boolean[] - Retourne le tableau des naissances 
     *  "moisDonnerNaissanceTab"
     */
    public boolean[] getMoisDonnerNaissanceTab()
    {
        return moisDonnerNaissanceTab;
    }

    /*
     * resetMoisDonnerNaissance()
     * Met l'attibut resetMoisDonnerNaissance à 0
     * Entrée : void
     * Sortie : void
     */
    public void resetMoisDonnerNaissance()
    {
        this.moisDonnerNaissance = 0;
    }

    /*
     * resetMoisDonnerNaissance()
     * Incrémente l'attibut moisDonnerNaissance
     * Entrée : void
     * Sortie : void
     */
    public void incrMoisDonnerNaissance()
    {
        this.moisDonnerNaissance++;
    }

    /*
     * setMoisDonnerNaissanceTab()
     * Remplace l'attribut moisNaissanceTab, 
     *  et met l'attibut moisDonnerNaissance à 0
     * Entrée : boolean[] moisNaissanceTab
     * Sortie : void
     */
    public void setMoisDonnerNaissanceTab(boolean[] moisNaissanceTab)
    {
        this.moisDonnerNaissanceTab = moisNaissanceTab;
        this.moisDonnerNaissance = 0;
    }


    /* 
     * Constructeur de la classe Lapin
    */
    public Lapin(Sexe sexe, int ageMaturiteSexuelle)
    {
        this.sexe = sexe;
        this.ageMaturiteSexuelle = ageMaturiteSexuelle;
    }
}
