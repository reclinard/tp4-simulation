import matplotlib.pyplot as plt
import math
import numpy as np

# N'oubliez pas de changer le filepath
fp = "C:\\xxxxxxxxx\\resultats"

"""
Ce programme python permet de générer les graphiques que j'ai utilisé dans mon rapport.
Il ne fait pas parti la simulation en tant que telle, et je l'inclus uniquement à titre indicatif.
Le code est fonctionnel, mais je n'ai pas cherché à le rendre propre, ni à appliquer les conventions
de style.
"""


def affiche_resultats():
	for num_simu in range(0, 5):
		donnees = [[] for i in range(5)]
		with open(f"{fp}/{num_simu}.txt", "r") as fichier:
			data = fichier.read().strip().split("\n")

		for j in range(15):
			donnees_ligne = data[j].strip().split(" ")
			for k in range(5):
				donnees[k].append(int(donnees_ligne[k]))

		x = [i for i in range(len(donnees[0]))]

		labels = ["Jeunes males", "Jeunes femelles", "Males adultes", "Femelles adultes", "Total"]
		colors = ["pink", "orange","green", "blue", "red"]
		for i in range(5):
			plt.plot(x, donnees[i], color=colors[i], label=labels[i])
		plt.xlabel("Time in months")
		plt.ylabel("Number of rabbits in each state")
		plt.legend(loc="upper left")
		plt.show()

def affiche_moyenne():
	nb_iterations = 61
	donnees = [[0 for i in range(nb_iterations)] for i in range(5)]
	print(donnees)

	for num_simu in range(0, 30):
		with open(f"{fp}/{num_simu}.txt", "r") as fichier:
			data = fichier.read().strip().split("\n")

		for j in range(nb_iterations):
			donnees_ligne = data[j].strip().split(" ")
			print(donnees_ligne)
			for k in range(5):
				donnees[k][j] += int(donnees_ligne[k])

	for i in range(len(donnees[0])):
		for k in range(5):
			donnees[k][i] /= nb_iterations


	x = [i for i in range(len(donnees[0]))]

	labels = ["Jeunes males", "Jeunes femelles", "Males adultes", "Femelles adultes", "Total"]
	colors = ["pink", "orange","green", "blue", "red"]
	for i in range(5):
		plt.plot(x, donnees[i], color=colors[i], label=labels[i])
	plt.xlabel("Time in months")
	plt.ylabel("Number of rabbits in each state")
	plt.legend(loc="upper left")
	plt.show()


def diagramme_nombre_total():
	donnees = []

	for num_simu in range(0, 30):
		with open(f"{fp}/{num_simu}.txt", "r") as fichier:
			data = fichier.read().strip().split("\n")

		donnees.append(int(data[-1].strip().split(" ")[4]))

	print(np.mean(donnees))
	print(np.std(donnees))

	labels = [str(i) for i in range(30)]
	plt.bar(labels, donnees, color="#86B8DC", width=0.2)
	plt.xlabel("Numéro de la simulation")
	plt.ylabel("Nombre de lapins")
	plt.show()

	
def affiche_resultats(num_simu):
	donnees = [[] for i in range(5)]
	with open(f"{fp}/{num_simu}.txt", "r") as fichier:
		data = fichier.read().strip().split("\n")

	for j in range(10):
		donnees_ligne = data[j].strip().split(" ")
		for k in range(5):
			donnees[k].append(int(donnees_ligne[k]))

	x = [i for i in range(len(donnees[0]))]

	labels = ["Jeunes males", "Jeunes femelles", "Males adultes", "Femelles adultes", "Total"]
	colors = ["pink", "orange","green", "blue", "red"]
	for i in range(5):
		plt.plot(x, donnees[i], color=colors[i], label=labels[i])
	plt.xlabel("Time in months")
	plt.ylabel("Number of rabbits in each state")
	plt.legend(loc="upper left")
	plt.show()


def histogramme():
	donnees = [0.1, 0.2, 0.4, 0.2, 0.1]
	labels = ["4", "5", "6", "7", "8"]
	colors = ["blue", "blue","blue", "blue", "blue"]
	plt.figure(figsize=(5, 4))
	plt.bar(labels, donnees, color="#86B8DC", width=0.4)
	plt.xlabel("Nombre de portées")
	plt.ylabel("Pourcentage")
	plt.show()



# affiche_resultats(3)
# affiche_resultats(10)